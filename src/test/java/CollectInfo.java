import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class CollectInfo {
    WebDriver driver;

    @BeforeTest
    public void setUpForAndroid() throws MalformedURLException {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("noReset", "true");
        dc.setCapability("appPackage", "com.truecaller");
        dc.setCapability("appActivity", "com.truecaller.ui.TruecallerInit");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
    }

    @Test
    public void findInfo() throws Exception {
        By searchField = By.xpath("//*[@text='search']");
        By searchField2 = By.id("search_field");
        String excelPath = "D:\\GitLab\\TrueCaller\\excelFiles\\PhoneNumber.xlsx";
        By profileAvatar = By.id("contact_photo");
        By name = By.id("name_or_number");
        By location = By.id("map_button_address_view");
        By emailAddress = By.id("buttonTextDetails");
        By profileAvatarView = By.id("avatar_view_img_avatar");

        for(int i = 0; i < 50; i ++){
            String phoneNumberStr = ExcelRead.readData(excelPath, "0", (i + 2), "2");
            System.out.println("Number: "+phoneNumberStr);

            driver.findElement(searchField).click();
            driver.findElement(searchField2).clear();
            Thread.sleep(1000);
            driver.findElement(searchField2).sendKeys(phoneNumberStr);
            Thread.sleep(3000);
            Thread.sleep(3000);

            if (driver.findElements(profileAvatar).size() > 0){
                System.out.println("Phone number found");

                driver.findElement(profileAvatar).click();
                Thread.sleep(2000);
                if (driver.findElements(profileAvatarView).size() > 0){
                    String nameStr = driver.findElement(name).getText();
                    System.out.println("Name: "+nameStr);

                }else {
                    System.out.println("Name not available :(");
                }

                if (driver.findElements(location).size() > 0){
                    String locationStr = driver.findElement(location).getText();
                    System.out.println("Location: "+locationStr);
                }else {
                    System.out.println("Location not found :(");
                }

                String emailAddressStr = driver.findElement(emailAddress).getText();
                System.out.println("EmailAddress: "+emailAddressStr);

                driver.navigate().back();
                Thread.sleep(1000);
                driver.navigate().back();
                driver.findElement(searchField);

            }else {
                System.out.println("Phone number not found");
            }
        }
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }
}

