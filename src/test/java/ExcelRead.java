import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Al Imran on 18/08/2018.
 */
public class ExcelRead {

    public static String readData(String excelPath, String sheetNum, int rowNum, String columnNum) throws IOException {
        File src = new File(excelPath);
        FileInputStream fis = new FileInputStream(src);

        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sh1 = wb.getSheetAt(Integer.parseInt(sheetNum));

        //Enable below like if required string reading
        //String data = sh1.getRow(rowNum).getCell(Integer.parseInt(columnNum)).getStringCellValue();

        //For reading numeric digit
        DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
        Cell cell = sh1.getRow(rowNum).getCell(Integer.parseInt(columnNum));
        String data = formatter.formatCellValue(cell);

        return data;
    }
}